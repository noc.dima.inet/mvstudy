<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Input;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;


class StoreArticleRequest extends FormRequest
{
   

    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => Str::slug($this->title),
            'published_at' => $this->clear_date 
                        ? $this->published_at = null
                        : date('Y-m-d', strtotime($this->published_at)),
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            // 'slug' => 'unique:articles',
            'slug' => [Rule::unique('articles')->ignore($this->slug,'slug')],
            'body' => 'required',
            'published_at' => 'nullable|date',
            'clear_date' => 'in:1',
        ];
    }

 
}
