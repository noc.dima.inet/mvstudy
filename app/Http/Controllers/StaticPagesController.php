<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\ {
    URL,
    View,
    Storage
};
use App\Models\Article;

class StaticPagesController extends Controller
{
   
    public function about()
    {
        return view('pages.about');
    }

    public function delivery() 
    {
        return view('pages.delivery');
    }
    
    public function contacts()
    {
        return view('pages.contacts');
    }

    public function pay()    
    {
        return view('pages.pay');
    }

    public function help()
    {
        return view('pages.help');
    }


}
