<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Article;
use App\Models\TestModel;
use App\Models\Product;


class HomeController extends Controller
{

    public function home() {

        $articles = Article::latest('published_at')
        ->limit(3)
        ->get();     

        $products = Product::latest('is_new')
        ->where('is_new', '!=', 0)
        ->limit(4)
        ->get();  

        return view('pages.home', [
            'articles' => $articles,
            'products'  => $products
        ]);
    }

}
