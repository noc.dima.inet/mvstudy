<?php

namespace App\Http\Controllers;
use Illuminate\Support\ {
    Collection,
    Arr,
    Str
};


use Illuminate\Http\Request;
use App\Models\ {
    Article,
    ProductColor,
    Manufacturer,
    Product
};
class ClientsController extends Controller
{
    public function clients()
    {
        $products = Product::with(['productColor', 'manufacturer'])->get() ;
        $avgPrice = $products->avg('price');
        $maxExpencivePrice = $products->max('price');

        $listColor =  $products->pluck('productColor.name')->unique();
        $listManuf = $products->pluck('manufacturer.name')->unique()->sort();

        $resSortColorWithSlug = $products->pluck('productColor.name')->unique()->sort()->keyBy(function ($item) {
            return Str::slug($item,'-');
        });
               
        // Сформируйте коллекцию товаров. В ней должны быть только товары со скидкой,а
        // также в названии этих товаров, или в году их выпуска, должна содержаться цифра 5 или 6.

        $filteredNameYearDiscont = $products->where('old_price','!=', null)->filter(function($value, $key )  {
            if(  Str::contains($value->year ,['6', '5']) || Str::contains($value->name,['6', '5']) )
                return $value;
        });

        // Сформируйте коллекцию всех производителей отсортированных по возрастанию средней цены 
        // (для товаров, без учета скидок), где ключом является название производителя, а 
        // значением средняя цена на товары этого производителя. При этом не должны учитываться товары, 
        // у которых не указан размер.

        $listManufGroup = $products->groupBy('manufacturer.name');        

        $avgPriceListManuf = $listManufGroup->map(function ($item, $key) {
            return $item->whereNotNull('size')->avg('price') ;
        });

        $resAvg=$avgPriceListManuf->sort();

        return view('pages.clients', [
            'avgPrice'          => $avgPrice,
            'maxExpencivePrice' => $maxExpencivePrice,
            'listColor'         => $listColor,
            'listManuf'         => $listManuf,
            'products'          => $products,
            'resSortColorWithSlug' => $resSortColorWithSlug,
            'filteredNameYearDiscont' => $filteredNameYearDiscont,
            'resAvg' =>  $resAvg,
        ]);
    }












}
