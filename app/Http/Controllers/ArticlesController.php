<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

use App\Http\Requests\StoreArticleRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Input;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Support\ValidatedInput;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     * отвечает за вывод главной инфо
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.articles',[
            'articles' => Article::where('published_at', '!=', null)
            ->latest('published_at')
            ->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * отвечает за отображение формы отображения
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create-article');
    }

    /**
     * Store a newly created resource in storage.
     * Сохраняет
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticleRequest $request)
    {
        $fields = $request->validated();

        Article::create($fields);

        return redirect(route('articles.index'));
    }

    /**
     * Display the specified resource.
     * показывает поста
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('pages.article', compact('article'), [
            'articleData' => $article,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * редактирование
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('pages.edit-article', compact('article'), [
            'articleData' => $article,
        ]);
    }

    /**
     * Update the specified resource in storage.
     * обновить
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(StoreArticleRequest $request, Article $article)
    {
        $fields = $request->validated();

        $article->update($fields);
        
        return redirect(route('articles.index'));

    }

    /**
     * Remove the specified resource from storage.
     * удаление поста
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        
        return redirect(route('articles.index'));
    }
}
