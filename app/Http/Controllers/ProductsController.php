<?php

namespace App\Http\Controllers;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function catalogShow() 
    {
        return view('pages.catalog',[
            'products' => Product::get()
        ]);
    }

    public function productShow($id)
    {  
        $product = Product::find($id)->load(['productColor', 'manufacturer']);

        return view('pages.product', [
            'product' => $product
        ]);

    }







}
