<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ {
    Article,
    ProductColor,
    Manufacturer,
    Product
};

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'body',
        'price',
        'old_price',
        'size',
        'manufacturer_id',
        'sound',
        'year',
        'color_id',
        'is_new',
    ];

    // ProductColor::class,
    // Product::class,
    // Manufacturer::class,
    // manufacturer_id 
    // color_id

    public function productColor()
    {
        return $this->belongsTo(ProductColor::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class);
    }


}
