<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ {
    Article,
    ProductColor,
    Manufacturer,
    Product
};

class Manufacturer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function manufacturer()
    {
        return $this->hasMany(Product::class);
    }
            

}
