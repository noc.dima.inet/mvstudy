<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/',  [ HomeController::class, 'home' ])->name('home');;

Route::get('/about', [ StaticPagesController::class, 'about'])->name('about');
Route::get('/delivery', [ StaticPagesController::class, 'delivery'])->name('delivery');
Route::get('/contacts', [ StaticPagesController::class, 'contacts'])->name('contacts');
Route::get('/pay', [ StaticPagesController::class, 'pay'])->name('pay');
Route::get('/help', [ StaticPagesController::class, 'help'])->name('help');

Route::resource('articles', ArticlesController::class)->scoped(['article' => 'slug']);

Route::get('/catalog', [ ProductsController::class, 'catalogShow' ])->name('catalog');
Route::get('/product/{id}', [ ProductsController::class, 'productShow'])->name('product');

Route::get('/clients', [ClientsController::class, 'clients'])->name('clients');



