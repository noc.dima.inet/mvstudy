<!doctype html />
<html class="antialiased" lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link href="{{asset('css/form.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/tailwind.css')}}" rel="stylesheet" />
    <link href="{{asset('css/base.css')}}" rel="stylesheet" />
    <link href="{{asset('css/main_page_template_styles.css')}}" rel="stylesheet" />
    <link href="{{asset('css/inner_page_template_styles.css')}}" rel="stylesheet" />

    <script src="{{asset('js/vendor/jquery-3.6.0.min.js')}}"></script>
    <link href="{{asset('js/vendor/slick.css')}}" rel="stylesheet">
    <script src="{{asset('js/vendor/slick.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>


    <title> @yield('title') </title>
    <link href="{{asset('favicon.ico')}}" rel="shortcut icon" type="image/x-icon">
</head>
<body class="bg-white text-gray-600 font-sans leading-normal text-base tracking-normal flex min-h-screen flex-col">
<div class="wrapper flex flex-1 flex-col">
    
    <x-header />
     
    @yield('content')
    
    <x-footer />

</div>

</body>
</html>