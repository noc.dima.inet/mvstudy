@section('title', 'Весь каталог компании')
@section('h1', 'Каталог')

@extends('layouts.app')


@section('content')
<x-breadcrumbs/>

 <main class="flex-1 container mx-auto bg-white flex">
    <div class="p-4">
         <h1 class="text-black text-3xl font-bold mb-4">
            @yield('h1')
         </h1>

        @yield('inner-catalog')
    </div>     
</main> <!-- конец главный div каталога -->

     
 </div>
 
 </main>
 @endsection
 