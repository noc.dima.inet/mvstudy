@section('title', 'Все статьи компании')
@section('h1', 'Все статьи')

@extends('layouts.app')


@section('content')
<x-breadcrumbs/>

 <main class="flex-1 container mx-auto bg-white flex">
 
 <div class="flex-1 grid grid-cols-4 lg:grid-cols-5 border-b">
     <!-- левое меню -->
        @section('leftbar')
            <x-leftbar/>
        @show
     <!-- конец левое меню -->
  
     <div class="col-span-4 sm:col-span-3 lg:col-span-4 p-4">
         <h1 class="text-black text-3xl font-bold mb-4">
            @yield('h1')
         </h1>

        @yield('inner-content')
         
        </div> <!-- конец главный div всех статей -->

    </div>
 </div>
 
 </main>
 @endsection
 