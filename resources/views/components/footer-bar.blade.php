<!-- 
--      Хранится в resources/views/components/footer-bar.blade.php
--      Меню внизу страницы в футере
--      Статичные страницы 
-->
@props([
            'pasive' => 'text-gray-600 hover:text-blue-500',
            'active' => 'text-blue-500 cursor-default'
        ])
<nav>
    <ul class="list-inside  bullet-list-item">
        <li><a class="{{ (Request::is('about')    ? $active :  $pasive ) }}" href="{{ route('about') }}">О магазе</a></li>
        <li><a class="{{ (Request::is('contacts') ? $active :  $pasive ) }}" href="{{ route('contacts') }}">Контакты</a></li>
        <li><a class="{{ (Request::is('delivery') ? $active :  $pasive ) }}" href="{{ route('delivery') }}">Условия доставки</a></li>
        <li><a class="{{ (Request::is('pay')      ? $active :  $pasive ) }}" href="{{ route('pay') }}">Условия оплаты</a></li>
        <li><a class="{{ (Request::is('help')     ? $active :  $pasive ) }}" href="{{ route('help') }}">Помощь</a></li>
    </ul>
</nav>