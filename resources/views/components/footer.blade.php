<!-- Хранится в resources/views/components/footer.blade.php -->
 
<footer class="container mx-auto">
        <section class="block sm:flex bg-white px-4 sm:px-8 py-4">
            <div class="flex-1">
                <div>
                    <p class="inline-block text-3xl text-black font-bold mb-4">Наши магазины</p>
                    <span class="inline-block pl-1"> / <a href="shops.html" class="inline-block pl-1 text-gray-600 hover:text-blue-500"><b>Все</b></a></span>
                </div>
                
                <div class="grid gap-6 grid-cols-1 lg:grid-cols-2">
                    <div class="w-full flex">
                        <div class="h-48 lg:h-auto w-32 xl:w-48 flex-none text-center rounded-lg overflow-hidden">
                            <a class="block w-full h-full hover:opacity-75" href="shops.html">
                                <img src="/pictures/disney.jpg" class="w-full h-full object-cover" alt=""></a>
                        </div>
                        <div class="px-4 flex flex-col justify-between leading-normal">
                            <div class="mb-8">
                                <div class="text-black font-bold text-xl mb-2">
                                    <a class="hover:text-blue-500" href="shops.html">Магазин Disney</a>
                                </div>
                                <div class="text-base space-y-2">
                                    <p class="text-gray-400">Москва, ул. Сомнительная, дом 2</p>
                                    <p class="text-black">+7 495 000 00 00</p>
                                    <p class="text-sm">Часы работы:<br> c 9.00 до 21.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-full flex">
                        <div class="h-48 lg:h-auto w-32 xl:w-48 flex-none text-center rounded-lg overflow-hidden">
                            <a class="block w-full h-full hover:opacity-75" href="shops.html">
                                <img src="/pictures/hamleys.jpg" class="w-full h-full object-cover" alt=""></a>
                        </div>
                        <div class="px-4 flex flex-col justify-between leading-normal">
                            <div class="mb-8">
                                <div class="text-black font-bold text-xl mb-2">
                                    <a class="hover:text-blue-500" href="shops.html">Магазин Hamsley</a>
                                </div>
                                <div class="text-base space-y-2">
                                    <p class="text-gray-400">Москва, ул. Неизвестная, дом 13</p>
                                    <p class="text-black">+7 495 000 00 00</p>
                                    <p class="text-sm">Часы работы:<br> c 8.00 до 23.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-8 border-t sm:border-t-0 sm:mt-0 sm:border-l py-2 sm:pl-4 sm:pr-8">
                <p class="text-3xl text-black font-bold mb-4">Информация</p>
                <x-footer-bar/>
            </div>
        </section>


        <div class="space-y-4 sm:space-y-0 sm:flex sm:justify-between items-center py-6 px-2 sm:px-0">
            <div class="copy pr-8">© 2021 Интернет-магазин Ушки на Макушке.</div>
            <div class="text-right">
                <a href="https://mvsvolkov.ru" target="_blank" class="flex inline-block items-center space-x-2">
                    <span>Разработал Волков Михаил</span>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 text-red-500" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                    </svg>
                </a>
            </div>
        </div>
    </footer>
 