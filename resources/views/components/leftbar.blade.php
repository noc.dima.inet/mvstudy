<!-- 
--      Хранится в resources/views/panels/left-bar.blade.php
--      Левое меню
--      Статичные страницы 
-->
<aside class="hidden sm:block col-span-1 border-r p-4">
<nav>
    <ul class="text-sm">
        <li>
            
            @props([
                'pasive' => 'hover:text-blue-500',
                'active' => 'text-blue-500 cursor-default'
            ])
            
            <p class="text-xl text-black font-bold mb-4">Информация</p>
            <ul class="space-y-2">
                <li><a class =" {{ (Request::is('about') ? $active : $pasive) }} " href="{{ route('about') }}"> О магазине </a></li>
                <li><a class =" {{ (Request::is('contacts') ? $active : $pasive) }}" href="{{ route('contacts') }}"> Контакты </a></li>
                <li><a class =" {{ (Request::is('delivery') ? $active : $pasive) }}" href="{{ route('delivery') }}"> Условия доставки </a></li>
                <li><a class =" {{ (Request::is('pay') ? $active : $pasive) }} " href="{{ route('pay') }}"> Условия оплаты </a></li>
                <li><a class =" {{ (Request::is('help') ? $active : $pasive) }}" href="{{ route('help') }}"> Помощь </a></li>
            </ul>
        </li>
    </ul>
</nav>
</aside>
 