<br/>
<button type="submit" value="sendArt" class="inline-block {{ $class }} hover:bg-opacity-70
    focus:outline-none text-white font-bold py-2 px-4 rounded">
    {{ $name }}
</button>
 