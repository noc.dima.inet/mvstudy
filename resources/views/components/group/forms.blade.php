<div class="block">
    <label for="{{ $name }}" class="text-gray-700 font-bold"> {{ $label }} </label>
        {{ $slot }} 
    <x-group.error :name="$name" />
</div>	