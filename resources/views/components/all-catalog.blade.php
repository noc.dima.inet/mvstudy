<div class="grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-4 gap-6">
    @foreach ($products as $product )     
    <div class="bg-white w-full border border-gray-100 rounded overflow-hidden shadow-lg hover:shadow-2xl pt-4">
        <a class="block w-full h-40" href="{{ route('product', $product) }}">
            <img class="w-full h-full hover:opacity-90 object-cover" src="/pictures/squirrel.png" alt="Белый мишка"></a>
        <div class="px-6 py-4">
            <div class="text-black font-bold text-xl mb-2"><a class="hover:text-blue-500" href="{{ route('product', $product) }}">
                {{ $product->name }}</a></div>
            <p class="text-grey-darker text-base">

                @if(  $product->old_price == null )
                 <span class="inline-block">{{ $product->price }}</span>
                @else
                 <span class="inline-block">{{ $product->price }}</span>
                 <span class="inline-block line-through pl-6 text-gray-400">{{ $product->old_price }}</span> 
                @endif
                
            </p>
        </div>
    </div>
    @endforeach
</div>