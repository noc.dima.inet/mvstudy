 
@section('title','Редактировать  статью' )
@section('h1', 'Редактировать статью' )

@extends('layouts.inner')
 
@section('inner-content')
 
<div class="space-y-4">
 
<form action="{{ route('articles.update', $article) }}" class="" method="POST" >
 
    <x-group.forms label="Поменяйте title" name="title">
        <x-group.forms.inputs.text name="title"
                                   id="field_title"
                                   type="text"
                                   value="{{ $article->title }}"
                                   :error="$errors->first('title')"
                                   placeholder="Введите заголовок"/>
    </x-group.forms>
    <x-group.forms label="Поменяйте описание" name="description">
        <x-group.forms.inputs.text name="description"
                                   id="field_description"
                                   type="text"
                                   value="{{ $article->description }}"
                                   :error="$errors->first('description')"
                                   placeholder="Введите описание"/>
    </x-group.forms>
    <x-group.forms label="Поменяйте текст" name="body">
        <x-group.forms.inputs.textarea name="body"
                                   id="field_body"
                                   type="text"
                                   value="{{ $article->body }}"
                                   :error="$errors->first('body')"
                                   placeholder="Введите текст"/>
    </x-group.forms>
    <x-group.forms label="Поменяйте дату публикации" name="published_at">
        <x-group.forms.inputs.date name="published_at"
                                   id="field_date"
                                   type="date"
                                   value="{{ $article->published_at }}"
                                   :error="$errors->first('published_at')"
                                   />
    </x-group.forms> 
    <x-group.forms label="Без даты" name="clear_date">
        <x-group.forms.inputs.checkbox name="clear_date"
                                   id="field_checkbox"
                                   type="checkbox"
                                   :value="1"
                                   label="Без даты"
                                   :error="$errors->first('clear_date')"
                                   />
    </x-group.forms>   
    {{csrf_field()}}
    @method('PUT')
    <x-group.forms.btn name="Сохранить изменения" class="bg-blue-500"/>
    </form>

    <form method="POST" action="{{ route('articles.destroy',$article) }}">
        <button type="submit" 
            class="inline-block bg-red-400 hover:bg-opacity-70
            focus:outline-none text-white font-bold py-2 px-4 rounded">
            Удалить навсегда
        </button>
        @method('DELETE')
        {{csrf_field()}}
    </form>
    
    <a href= "{{ route('articles.show', $article) }}" type="submit" value="cancel" 
        class="inline-block bg-gray-400 hover:bg-opacity-70
        focus:outline-none text-white font-bold py-2 px-4 rounded">
        Отмена
    </a>




</div> <!-- fin space-y-4 -->
 
@endsection
 