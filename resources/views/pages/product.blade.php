 
@section('title',$product->name)
@section('h1', $product->name)

@extends('layouts.catalog')
@section('inner-catalog')

<div class="space-y-4">
    <!-- блок конкретного продукта -->
    <div class="flex-1 grid grid-cols-1 lg:grid-cols-5 border-b w-full">
        <div class="col-span-3 border-r-0 sm:border-r pb-4 px-4 text-center catalog-detail-slick-preview" data-slick-carousel-detail>
            <div class="mb-4 border rounded" data-slick-carousel-detail-items>
                <img class="w-full" src="/pictures/bear_1.png" alt="" title="">
                {{-- <img class="w-full" src="/pictures/bear_1_1.png" alt="" title="">
                <img class="w-full" src="/pictures/bear_1_2.png" alt="" title="">
                <img class="w-full" src="/pictures/bear_1_3.png" alt="" title=""> --}}
            </div>
            <div class="flex space-x-4 justify-center items-center" data-slick-carousel-detail-pager>
            </div>
        </div>
        <div class="col-span-1 lg:col-span-2">
            <div class="space-y-4 w-full">
                <div class="block px-4">
                   @if(  $product->old_price == null )
                    <p class="font-bold text-2xl text-blue-500">{{ $product->price }} </p>
                   @else
                    <p class="text-base line-through text-gray-400">{{ $product->old_price }} </p>
                    <p class="font-bold text-2xl text-blue-500">{{ $product->price }} </p>
                   @endif
                   <div class="mt-4 block">
                        <form>
                            <button class="inline-block bg-blue-500 hover:bg-opacity-70 focus:outline-none text-white font-bold py-2 px-4 rounded">
                                <svg xmlns="http://www.w3.org/2000/svg" class="inline-block h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                                </svg>
                                Купить
                            </button>
                        </form>
                    </div>
                </div>
                <div class="block border-t clear-both w-full" data-accordion data-active>
                    <div class="text-black text-2xl font-bold flex items-center justify-between hover:bg-gray-50 p-4 cursor-pointer" data-accordion-toggle>
                        <span>Параметры</span>
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-blue-500 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" data-accordion-not-active style="display: none">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-blue-500 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" data-accordion-active>
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                        </svg>
                    </div>
                    
                    <div class="my-4 px-4" data-accordion-details>
                        <table class="w-full">
                            @if($product->size !== null )
                             <tr>
                                <td class="py-2 text-gray-600 w-1/2">Высота:</td>
                                <td class="py-2 text-black font-bold w-1/2">{{ $product->size }}</td>
                             </tr>
                            @endif
                            @if($product->manufacturer->name !== null )
                            <tr>
                                <td class="py-2 text-gray-600 w-1/2">Производитель:</td>
                                <td class="py-2 text-black font-bold w-1/2">{{ $product->manufacturer->name }}</td>
                            </tr>
                            @endif
                            @if($product->sound !== null )
                             <tr>
                                <td class="py-2 text-gray-600 w-1/2">Звук:</td>
                                <td class="py-2 text-black font-bold w-1/2">{{ $product->sound}}</td>
                             </tr>
                            @endif
                            <tr>
                                <td class="py-2 text-gray-600 w-1/2">Год выпуска:</td>
                                <td class="py-2 text-black font-bold w-1/2">{{ $product->year }}</td>
                            </tr>
                            <tr>
                                <td class="py-2 text-gray-600 w-1/2">Цвет:</td>
                                <td class="py-2 text-black font-bold w-1/2"> {{ $product->productColor->name }}</td>
                            </tr>
 
                        </table>
                    </div>
                </div>
                <div class="block border-t clear-both w-full" data-accordion>
                    <div class="text-black text-2xl font-bold flex items-center justify-between hover:bg-gray-50 p-4 cursor-pointer" data-accordion-toggle>
                        <span>Описание</span>
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-blue-500 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" data-accordion-not-active>
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" class="text-blue-500 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" data-accordion-active style="display: none">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                        </svg>
                    </div>
                    <div class="my-4 px-4 space-y-4" data-accordion-details style="display: none">
                        {!! $product->body !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- конец конкретного продукта -->

    
@endsection