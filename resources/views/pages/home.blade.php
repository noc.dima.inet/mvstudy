
@section('title', 'Главные уши, ум и совесть')

@extends('layouts.app')

@section('content')
<main class="flex-1 container mx-auto bg-white">
    <section class="bg-white">

        <div data-slick-carousel>
            <div class="relative banner">
                <div class="w-full h-full bg-black"><img class="w-full h-full object-cover object-center opacity-70" src="pictures/test_banner_1.jpg" alt="" title=""></div>
                <div class="absolute top-0 left-0 w-full px-10 py-4 sm:px-20 sm:py-8 lg:px-40 lg:py-10">
                    <h1 class="text-gray-100 text-lg sm:text-2xl md:text-4xl xl:text-6xl leading-relaxed sm:leading-relaxed md:leading-relaxed xl:leading-relaxed font-bold uppercase">Мир чудес в Disney Land</h1>
                    <h2 class="text-gray-200 italic text-xs sm:text-lg md:text-xl xl:text-3xl leading-relaxed sm:leading-relaxed md:leading-relaxed xl:leading-relaxed font-bold">А по темным улицам гуляет дождь! Зигварт считал критерием истинности необходимость и&nbsp;общезначимость, для&nbsp;которых нет никакой опоры в&nbsp;объективном мире <a href="#" class="text-blue-500 hover:opacity-75">подробнее</a></h2>
                </div>
            </div>
            <div class="relative banner">
                <div class="w-full h-full bg-black"><img class="w-full h-full object-cover object-center opacity-70" src="pictures/test_banner_2.jpg" alt="" title=""></div>
                <div class="absolute top-0 left-0 w-full px-10 py-4 sm:px-20 sm:py-8 lg:px-40 lg:py-10">
                    <h1 class="text-gray-100 text-lg sm:text-2xl md:text-4xl xl:text-6xl leading-relaxed sm:leading-relaxed md:leading-relaxed xl:leading-relaxed font-bold uppercase">Найди своего персонажа в The Simpsons</h1>
                    <h2 class="text-gray-200 italic text-xs sm:text-lg md:text-xl xl:text-3xl leading-relaxed sm:leading-relaxed md:leading-relaxed xl:leading-relaxed font-bold">Аподейктика индуктивно подчеркивает катарсис, однако Зигварт считал критерием истинности необходимость и&nbsp;общезначимость, для&nbsp;которых нет никакой опоры в&nbsp;объективном мире <a href="#" class="text-blue-500 hover:opacity-75">подробнее</a></h2>
                </div>
            </div>
            <div class="relative banner">
                <div class="w-full h-full bg-black"><img class="w-full h-full object-cover object-center opacity-70" src="pictures/test_banner_3.jpg" alt="" title=""></div>
                <div class="absolute top-0 left-0 w-full px-10 py-4 sm:px-20 sm:py-8 lg:px-40 lg:py-10">
                    <h1 class="text-gray-100 text-lg sm:text-2xl md:text-4xl xl:text-6xl leading-relaxed sm:leading-relaxed md:leading-relaxed xl:leading-relaxed font-bold uppercase">Успей съесть последний орех</h1>
                    <h2 class="text-gray-200 italic text-xs sm:text-lg md:text-xl xl:text-3xl leading-relaxed sm:leading-relaxed md:leading-relaxed xl:leading-relaxed font-bold">Аподейктика индуктивно подчеркивает катарсис, однако Зигварт считал критерием истинности необходимость и&nbsp;общезначимость, для&nbsp;которых нет никакой опоры в&nbsp;объективном мире <a href="#" class="text-blue-500 hover:opacity-75">подробнее</a></h2>
                </div>
            </div>
        </div>
    </section>
    <section class="pb-4 px-6">
 
        {{-- вывод 4-х позиций товара --}}
        @if($products->isNotEmpty())
            <p class="inline-block text-3xl text-black font-bold mb-4">
                <a href="{{ route('catalog') }}">Товары недели</a>
            </p>
            <x-home-new-products :products="$products"/>  
        @endif 
        {{-- конец вывод 4-х позиций товара --}}
    

    </section>

    <section class="bg-gray-900 px-6 py-4">
        <div>
            <p class="inline-block text-3xl text-white font-bold mb-4">Статьи</p>
            <span class="inline-block text-gray-200 pl-1"> / 
                <a href="{{ route('articles.index') }}" class="inline-block pl-1 text-gray-200 hover:text-blue-500">
                    <b>Все</b>
                </a>
            </span>
        </div>
        <!-- 
            Блок вывода послежних 3-х статей
                -- компонент three-articles.blade
        -->

        <x-home-articles :articles="$articles"/>

    </section>        
</main>
@endsection

 
