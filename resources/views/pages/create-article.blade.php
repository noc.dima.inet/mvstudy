 
@section('title','Создать новую статью' )
@section('h1', 'Создать новую статью' )

@extends('layouts.inner')
 
@section('inner-content')
 
<div class="space-y-4">
    
<form action="{{ route('articles.store') }}" class="" method="POST" >
    @error('slug')
    <x-group.message.mistake label="Статья с таким заголовком уже есть" :error="$errors"/> 
    @enderror
    <x-group.forms label="Введите title" name="title">
        <x-group.forms.inputs.text name="title"
                                   id="field_title"
                                   type="text"
                                   :value="old('title')"
                                   :error="$errors->first('title')"
                                   placeholder="Введите заголовок"/>
    </x-group.forms>
    <x-group.forms label="Введите описание" name="description">
        <x-group.forms.inputs.text name="description"
                                   id="field_description"
                                   type="text"
                                   :value="old('description')"
                                   :error="$errors->first('description')"
                                   placeholder="Введите описание"/>
    </x-group.forms>
    <x-group.forms label="Введите текст" name="body">
        <x-group.forms.inputs.textarea name="body"
                                   id="field_body"
                                   type="text"
                                   :value="old('body')"
                                   :error="$errors->first('body')"
                                   placeholder="Введите текст"/>
    </x-group.forms>
    <x-group.forms label="Введите дату публикации" name="published_at">
        <x-group.forms.inputs.date name="published_at"
                                   id="field_date"
                                   type="date"
                                   :value="old('published_at')"
                                   :error="$errors->first('published_at')"
                                   />
    </x-group.forms> 
    <x-group.forms label="Без даты" name="clear_date">
        <x-group.forms.inputs.checkbox name="clear_date"
                                   id="field_checkbox"
                                   type="checkbox"
                                   :value="1"
                                   label="Без даты"
                                   :error="$errors->first('clear_date')"
                                   />
    </x-group.forms>   
    {{csrf_field()}}
    <x-group.forms.btn name="Отправить" class="bg-blue-500"/>
    <x-group.inputs.btn-grey name="Отмена"/>

</form>

</div> <!-- fin space-y-4 -->

<div class="mt-4" > 
    <a class="inline-flex items-center text-blue-500 hover:opacity-75" href="{{ route('articles.index') }}">
        <svg xmlns="http://www.w3.org/2000/svg" class="inline-block h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 16l-4-4m0 0l4-4m-4 4h18" />
        </svg>
        К списку статей
    </a>
</div>
 
@endsection
 