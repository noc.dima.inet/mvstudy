 
@section('title',"Страница клиенты")
@section('h1', "Страница клиенты")

@extends('layouts.inner')
 
@section('inner-content')
 
    <div class="space-y-4">
        <p>Среднняя цена: <b>{{ $avgPrice }}</b></p>
        <p>Максимальная цена: <b>{{ $maxExpencivePrice }}</b></p>
        <p>Все виды цветов товаров:
            @foreach($listColor as $color)
             {{ $color}}, 
            @endforeach
        </p>
        <p>Все виды производителей:
            @foreach($listManuf as $manuf)
             {{ $manuf }}, 
            @endforeach
        </p>
        <p> @foreach($resSortColorWithSlug as $slug=>$key)
            {{ $slug }}  => {{ $key }} || 
           @endforeach
        </p>          
        <p>товары со скидкой, в названии  или в году их выпуска  содержется цифра 5 или 6:<br/>
            @foreach($filteredNameYearDiscont as $filteredNYD)
            {{ $filteredNYD->name }} г.в. {{ $filteredNYD->year }}  || 
           @endforeach
        </p>   
        <p> Название производителя =>  значением средняя цена на товары 
            этого производителя (без размера)
          <ul>
           <p>Ср.цена производителей продуктов где указан размер:</p> 
             @foreach($resAvg as $res => $key)
             <li> {{ $res }}  => Ср.цена: {{ $key }}  </li> 
           @endforeach
          </ul>
        </p>        
    </div>
  
@endsection