 
@section('title',$articleData->title )
@section('h1', $articleData->title )

@extends('layouts.inner')
 
@section('inner-content')

<div class="space-y-4">
 
    <img src="/pictures/cat_toy.png" alt="" title="">

        <!-- тело самой статьи -->
        {!! $articleData->body !!}
        <!-- конец тело самой статьи -->

    <div>
        <span class="text-sm text-white italic rounded bg-blue-500 px-2">Это</span>
        <span class="text-sm text-white italic rounded bg-blue-500 px-2">Теги</span>
    </div>
 
<a href= "{{ route('articles.edit', $article) }}" type="submit" value="cancel" 
class="inline-block bg-gray-400 hover:bg-opacity-70
focus:outline-none text-white font-bold py-2 px-4 rounded">
    Редактировать
</a>


</div> <!-- fin space-y-4 -->

<div class="mt-4">
    <a class="inline-flex items-center text-blue-500 hover:opacity-75" href="{{ route('articles.index') }}">
        <svg xmlns="http://www.w3.org/2000/svg" class="inline-block h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 16l-4-4m0 0l4-4m-4 4h18" />
        </svg>
        К списку статей
    </a>
</div>
 
@endsection
 