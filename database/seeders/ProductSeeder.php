<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\Manufacturer;
use App\Models\ProductColor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Sequence;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manuf = Manufacturer::all();
        $color = ProductColor::all();

        Product::factory()
            ->count(15)
            ->state(new Sequence(
                fn ($sequence) => ['manufacturer_id' => $manuf->random(),
                                   'product_сolor_id' => $color->random()],
            ))
            ->create();

    }
}
