<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\ProductColor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Sequence;

class ProductColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $productColorsArray = [
            [
                'name' =>'красный',
            ],
            [
                'name' =>'зеленый',
            ],
            [
                'name' =>'синий',
            ],
            [
                'name' =>'лиловый',
            ],           
            [
                'name' =>'ярко бежевый',
            ],   
            [
                'name' =>'ярко бирюзовый',
            ], 
        ];
        
        foreach ($productColorsArray as $productColors) {
            ProductColor::factory()
            ->create($productColors);
        }
    }
}
