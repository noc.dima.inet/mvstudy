<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Manufacturer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Sequence;

class ManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $productColorsArray = [
            [
                'name' =>'ВинниПухово',
            ],
            [
                'name' =>'Страна Игр',
            ],
            [
                'name' =>'UEK',
            ],
            [
                'name' =>'Чебурашка КБ',
            ],           
            [
                'name' =>'Малявкино',
            ],   
            [
                'name' =>'Лаппи Тойз',
            ], 
        ];
        
        foreach ($productColorsArray as $productColors) {
            Manufacturer::factory()
            ->create($productColors);
        }
    }
}
