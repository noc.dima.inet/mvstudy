<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Database\Factories\ManufacturerFactory;
use App\Database\Factories\ProductColorFactory;
use App\Models\Manufacturer;
use App\Models\ProductColor;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $productSize  = ["12 см","22 см","112 см","34 см","53 см","71 см"];
        $productSound = ["Песни","Гармонь","Гитара","Вальс","Лирика","Колыбельные"];
        $productName  = ["Плюшевый мишка","Плюшевый слоник","Неваляшка","Винии Пух",
                        "Кукла Барби","Домовенок Кузя"];
        // dd(array_rand($productSound,1));

        return [
            'name' => $productName[array_rand($productName,1)],
            'body' => $this->faker->text(),
            'price' => $this->faker->numberBetween(1000, 3000),
            'old_price' => $this->faker->boolean() ? $this->faker->numberBetween(3000, 4000) : null,
            'size' => $productSize[array_rand($productSize,1)],
            'manufacturer_id' => Manufacturer::factory(),
            'sound' => $productSound[array_rand($productSound,1)],
            'year' => $this->faker->numberBetween(2019, 2021),
            'product_сolor_id' => ProductColor::factory(),
            'is_new' => $this->faker->boolean(),
        ];
    }
}
