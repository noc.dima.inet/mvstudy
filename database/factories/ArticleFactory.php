<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'        => $this->faker->sentence(),
            'description'  => $this->faker->paragraph(),
            'body'         => $this->faker->text(),
            'slug'         => $this->faker->unique()->slug(),
            'published_at' => $this->faker->boolean() ? $this->faker->dateTimeThisMonth() : null
        ];
    }
    
    public function published_at()
    {
        return $this->faker->dateTimeThisMonth();
    }

}
